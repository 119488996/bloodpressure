/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
Edited by 119488996
 */
package com.simonwoodworth.bloodpressure;
public class Patient {
    private String name;
    private int age;
    private char gender; // M for male, F for female, or other initials as needed.
    private int heartRate;
    private double sbp; // Systolic Blood Pressure
    private double dbp; // Diastolic Blood Pressure
    private double map; // Mean Arterial Pressure

    // Constructor for the Patient class.
    public Patient(String name, int age, char gender, int heartRate, double sbp, double dbp) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.heartRate = heartRate;
        this.sbp = sbp;
        this.dbp = dbp;
        this.map = BloodPressureCalculator.calculateMAP(sbp, dbp); // Calculate MAP using the BloodPressureCalculator class.
    }

    // Getters and setters for the attributes.
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public double getSBP() {
        return sbp;
    }

    public void setSBP(double sbp) {
        this.sbp = sbp;
        this.map = BloodPressureCalculator.calculateMAP(sbp, this.dbp); // Recalculate MAP if SBP changes.
    }

    public double getDBP() {
        return dbp;
    }

    public void setDBP(double dbp) {
        this.dbp = dbp;
        this.map = BloodPressureCalculator.calculateMAP(this.sbp, dbp); // Recalculate MAP if DBP changes.
    }

    public double getMAP() {
        return map;
    }

    // A toString method to return information about the patient.
    @Override
    public String toString() {
        return "Patient{" +
               "Name='" + name + '\'' +
               ", Age=" + age +
               ", Gender=" + gender +
               ", Heart Rate=" + heartRate +
               ", SBP=" + sbp +
               ", DBP=" + dbp +
               ", MAP=" + map +
               '}';
    }
}
